## NO 1
![Diagram_Database_Genshin_Impact](/uploads/5ba7b7826925df969b7c74c5243df284/Diagram_Database_Genshin_Impact.png)

## NO 2
```
CREATE TABLE `player` (
  `id_player` int PRIMARY KEY,
  `username_player` varchar(255),
  `level_player` int,
  `exp_level_player` int,
  `signatur` varchar(255)
);

CREATE TABLE `character` (
  `id_character` int PRIMARY KEY,
  `name_character` varchar(255),
  `region` varchar(255),
  `type_weapon` varchar(255),
  `character_star` int,
  `element_character` varchar(255)
);

CREATE TABLE `player_character` (
  `id_player_character` int PRIMARY KEY,
  `id_player` int,
  `id_character` int
);

CREATE TABLE `character_stats` (
  `id_character` int,
  `level_character` int,
  `max_hp` int,
  `atk` int,
  `def` int,
  `elemntal_mastery` int
);

CREATE TABLE `player_character_stats` (
  `id_player_character` int,
  `level_character` int,
  `max_hp` int,
  `atk` int,
  `def` int,
  `elemntal_mastery` int
);

CREATE TABLE `player_weapon` (
  `id_player` int,
  `id_weapon` int
);

CREATE TABLE `weapon` (
  `id_weapon` int PRIMARY KEY,
  `type_weapon` varchar(255),
  `name_weapon` varchar(255),
  `main_stats_weapon` varchar(255),
  `weapon_star` int
);

CREATE TABLE `player_character_weapon` (
  `id_player_character` int,
  `id_weapon` int,
  `type_weapon` varchar(255),
  `name_weapon` varchar(255),
  `level_weapon` int,
  `refine` int
);

CREATE TABLE `main_character` (
  `id_main_character` int PRIMARY KEY,
  `name_main_character` varchar(255),
  `gender_main_character` varchar(255)
);

CREATE TABLE `player_artefak` (
  `id_player` int,
  `id_artefak` int
);

CREATE TABLE `artefak` (
  `id_artefak` int PRIMARY KEY,
  `name_artefak` varchar(255),
  `pasif_2` text,
  `pasif_4` text,
  `story_artefak` text
);

CREATE TABLE `player_character_artefak` (
  `id_player_character` int,
  `id_artefak` int,
  `name_artefak` varchar(255),
  `main_stats_artefak` varchar(255)
);

CREATE TABLE `character_talent` (
  `id_character_talent` int PRIMARY KEY,
  `normal_attack` int,
  `elemntal_skill` int,
  `elemntal_burst` int,
  `pasif_skill_1` text,
  `pasif_skill_2` text
);

CREATE TABLE `player_character_talent` (
  `id_player_character` int,
  `normal_attack` int,
  `elemntal_skill` int,
  `elemntal_burst` int,
  `pasif_skill_1` text,
  `pasif_skill_2` text
);

CREATE TABLE `region` (
  `id_region` int PRIMARY KEY,
  `name_region` varchar(255),
  `name_archon` varchar(255),
  `elemnt_region` varchar(255),
  `music_theme` varchar(255)
);

CREATE TABLE `elemntal` (
  `id_elemental` int PRIMARY KEY,
  `name_elemental` varchar(255),
  `type_elemental` varchar(255),
  `weakness` varchar(255),
  `strength` varchar(255)
);

CREATE TABLE `elemntal_reaction` (
  `id_elemntal_reaction` int PRIMARY KEY,
  `name_elemntal_reaction` varchar(255),
  `description_reaction` text,
  `reactants` varchar(255),
  `effects_reactants` text
);

CREATE TABLE `quest` (
  `id_quest` int PRIMARY KEY,
  `name_quest` varchar(255),
  `type_quest` varchar(255),
  `story_quest` text,
  `reward_quest` varchar(255),
  `start_condition` text,
  `description` text
);

CREATE TABLE `enemy` (
  `id_enemy` int PRIMARY KEY,
  `name_enemy` varchar(255),
  `location_enemy` varchar(255),
  `element_enemy` varchar(255),
  `type_enemy` varchar(255),
  `difficulty_enemy` varchar(255),
  `story_enemy` text,
  `drop_item` varchar(255)
);

CREATE TABLE `oculus` (
  `id_oculus` int PRIMARY KEY,
  `name_oculus` varchar(255),
  `origin_regional` varchar(255),
  `total_oculus` int,
  `location_oculus` varchar(255)
);

CREATE TABLE `genesis_topup` (
  `id_genesis_topup` int PRIMARY KEY,
  `id_player` int,
  `topup_amount` int,
  `topup_time` datetime
);

CREATE TABLE `genesis_store` (
  `id_genesis` int PRIMARY KEY,
  `id_skin` int,
  `name_skin` varchar(25),
  `price_skin` int,
  `Purchase_date` datetime
);

CREATE TABLE `wish` (
  `id_wish` int PRIMARY KEY,
  `id_player` int,
  `type_wish` varchar(25),
  `wish_time` datetime,
  `item_name` varchar(255),
  `rarity` int
);

CREATE TABLE `world` (
  `id_player` int,
  `id_region` int,
  `id_elemental` int,
  `id_quest` int
);

ALTER TABLE `player_character` ADD FOREIGN KEY (`id_character`) REFERENCES `character` (`id_character`);

ALTER TABLE `player_character` ADD FOREIGN KEY (`id_player`) REFERENCES `player` (`id_player`);

ALTER TABLE `player_character_stats` ADD FOREIGN KEY (`id_player_character`) REFERENCES `player_character` (`id_player_character`);

ALTER TABLE `character_stats` ADD FOREIGN KEY (`id_character`) REFERENCES `character` (`id_character`);

ALTER TABLE `player_character_weapon` ADD FOREIGN KEY (`id_player_character`) REFERENCES `player_character` (`id_player_character`);

ALTER TABLE `main_character` ADD FOREIGN KEY (`id_main_character`) REFERENCES `player` (`id_player`);

ALTER TABLE `player_weapon` ADD FOREIGN KEY (`id_player`) REFERENCES `player` (`id_player`);

ALTER TABLE `player_weapon` ADD FOREIGN KEY (`id_weapon`) REFERENCES `weapon` (`id_weapon`);

ALTER TABLE `character_talent` ADD FOREIGN KEY (`id_character_talent`) REFERENCES `character` (`id_character`);

ALTER TABLE `player_character_talent` ADD FOREIGN KEY (`id_player_character`) REFERENCES `player_character` (`id_player_character`);

ALTER TABLE `player_character_weapon` ADD FOREIGN KEY (`id_player_character`) REFERENCES `weapon` (`id_weapon`);

ALTER TABLE `player_artefak` ADD FOREIGN KEY (`id_player`) REFERENCES `player` (`id_player`);

ALTER TABLE `player_artefak` ADD FOREIGN KEY (`id_artefak`) REFERENCES `artefak` (`id_artefak`);

ALTER TABLE `player_character_artefak` ADD FOREIGN KEY (`id_player_character`) REFERENCES `player_character` (`id_player_character`);

ALTER TABLE `player_character_artefak` ADD FOREIGN KEY (`id_artefak`) REFERENCES `artefak` (`id_artefak`);

ALTER TABLE `genesis_topup` ADD FOREIGN KEY (`id_player`) REFERENCES `player` (`id_player`);

ALTER TABLE `genesis_store` ADD FOREIGN KEY (`id_genesis`) REFERENCES `genesis_topup` (`id_genesis_topup`);

ALTER TABLE `wish` ADD FOREIGN KEY (`id_player`) REFERENCES `player` (`id_player`);

ALTER TABLE `wish` ADD FOREIGN KEY (`id_wish`) REFERENCES `genesis_topup` (`id_genesis_topup`);

ALTER TABLE `player` ADD FOREIGN KEY (`id_player`) REFERENCES `world` (`id_player`);

ALTER TABLE `region` ADD FOREIGN KEY (`id_region`) REFERENCES `world` (`id_region`);

ALTER TABLE `enemy` ADD FOREIGN KEY (`id_enemy`) REFERENCES `region` (`id_region`);

ALTER TABLE `elemntal` ADD FOREIGN KEY (`id_elemental`) REFERENCES `world` (`id_elemental`);

ALTER TABLE `elemntal_reaction` ADD FOREIGN KEY (`id_elemntal_reaction`) REFERENCES `elemntal` (`id_elemental`);

ALTER TABLE `region` ADD FOREIGN KEY (`id_region`) REFERENCES `oculus` (`id_oculus`);

ALTER TABLE `quest` ADD FOREIGN KEY (`id_quest`) REFERENCES `world` (`id_quest`);
```




## NO 3
```
INSERT INTO weapon (id_weapon, type_weapon, name_weapon, main_stats_weapon, weapon_star) VALUES
(1, 'Sword', 'Lion\'s Roar', 'ATK%', 4),
(2, 'Sword', 'Aquila Favonia', 'ATK%', 5),
(3, 'Sword', 'The Black Sword', 'CRIT Rate%', 4),
(4, 'Sword', 'Skyward Blade', 'Energy Recharge%', 5),
(5, 'Sword', 'Festering Desire', 'Energy Recharge%', 4),
(6, 'Sword', 'Sword of Descension', 'ATK%', 3),
(7, 'Sword', 'Harbinger of Dawn', 'CRIT DMG%', 3),
(8, 'Sword', 'Favonius Sword', 'Energy Recharge%', 4),
(9, 'Sword', 'Prototype Rancour', 'ATK%', 4),
(10, 'Sword', 'Iron Sting', 'Elemental Mastery', 4),
(11, 'Claymore', 'Wolf\'s Gravestone', 'ATK%', 5),
(12, 'Claymore', 'Skyward Pride', 'Energy Recharge%', 5),
(13, 'Claymore', 'The Bell', 'HP%', 4),
(14, 'Claymore', 'Prototype Archaic', 'ATK%', 4),
(15, 'Claymore', 'Serpent Spine', 'CRIT Rate%', 4),
(16, 'Claymore', 'Rainslasher', 'Elemental Mastery', 4),
(17, 'Claymore', 'Bloodtainted Greatsword', 'Physical DMG%', 3),
(18, 'Claymore', 'Whiteblind', 'DEF%', 4),
(19, 'Polearm', 'Vortex Vanquisher', 'ATK%', 5),
(20, 'Polearm', 'Primordial Jade Winged-Spear', 'ATK%', 5);
```


```
UPDATE weapon
SET type_weapon = 'Bow'
WHERE id_weapon = '5';
```


```
INSERT INTO genesis_topup (id_genesis_topup, id_player, topup_amount, topup_time)
VALUES
(3, 1003, 4342965, '2023-02-19 10:15:00'),
(4, 1004, 72275, '2022-09-29 11:20:00'),
(5, 1005, 217355, '2022-12-11 12:30:00'),
(6, 1006, 722355, '2022-03-15 13:00:00'),
(7, 1007, 14355, '2022-06-28 14:15:00'),
(8, 1008, 72275, '2022-09-01 15:30:00'),
(9, 1009, 434655, '2023-01-01 16:45:00'),
(10, 1010, 1447655, '2023-07-11 17:00:00'),
(11, 1011, 72275, '2022-11-12 18:15:00'),
(12, 1012, 217355, '2022-01-12 19:30:00'),
(13, 1013, 578000, '2022-02-04 20:45:00'),
(14, 1014, 72275, '2022-08-07 21:00:00'),
(15, 1015, 217355, '2022-05-21 22:15:00'),
(16, 1016, 289531, '2022-12-22 23:30:00'),
(17, 1017, 434655, '2022-10-26 00:45:00'),
(18, 1018, 722355, '2022-02-22 01:00:00'),
(19, 1019, 14355, '2022-04-24 02:15:00'),
(20, 1020, 1447655, '2023-13-18 03:30:00');
```


```
ALTER TABLE weapon 
MODIFY story_weapon varchar(255);
ALTER TABLE weapon 
ADD story_weapon text;
```


```
DELETE FROM weapon 
WHERE id_weapon = '4';
```

```
ALTER TABLE quest 
RENAME COLUMN description 
TO conditions;
```


## NO 4
 1. Bagaimana cara melihat keuntungan dari hasil topup genesis para player pertahun
```
SELECT 
  YEAR(topup_time) AS year, 
  SUM(topup_amount) AS total_topup_IDR
FROM genesis_topup 
GROUP BY YEAR(topup_time);
```

2. Bagaimana cara melihat skin apa yang paling banyak di beli
```
SELECT s.name_skin, COUNT(*) as total_pembelian
FROM purchase p
JOIN skin s ON p.id_skin = s.id_skin
GROUP BY s.name_skin
ORDER BY total_pembelian DESC
LIMIT 1;
```


3. Bagaimana cara menghitung rata rata player topup genesis tiap tahun 
```
SELECT YEAR(topup_time) AS tahun, AVG(topup_amount) AS rata_rata_topup_IDR
FROM genesis_topup
GROUP BY YEAR(topup_time);
```


4. Bagaimana cara mengetahui player masih main dan yang sudah tidak main (30 hari tidak login di anggap sudah tidak main)
```
SELECT 
  username_player,
  CASE WHEN last_login_date >= DATE_SUB(NOW(), INTERVAL 30 DAY) 
  THEN 'Active' ELSE 'Inactive' END AS player_status
FROM player;
```


5. Bagaimana cara mengurutkan skin dari yang termahal sampai termurah dan sebalik nya
```
select name_skin, price_skin
from skin
order by price_skin;
```
```
select name_skin, price_skin
from skin
order by price_skin desc;
```

6. Bagaimana cara melihat detail jumlah dari skin yang terjual
```
SELECT s.name_skin, COUNT(*) as total_pembelian
FROM purchase p
JOIN skin s ON p.id_skin = s.id_skin
GROUP BY s.name_skin
ORDER BY total_pembelian ;
```

 
7. Berapa total pendapatan genesis dari setiap skin

```
SELECT s.name_skin, COUNT(*) as total_pembelian, SUM(s.price_skin) as total_pendapatan
FROM purchase p
JOIN skin s ON p.id_skin = s.id_skin
GROUP BY s.name_skin
ORDER BY total_pembelian DESC;
```


9. Berapa total dari setiap player topup
```
SELECT
    gp.id_player, p.username_player,
    SUM(topup_amount) AS total_topup
FROM genesis_topup gp
JOIN player p ON gp.id_player = p.id_player
GROUP BY gp.id_player
ORDER BY total_topup DESC;
```


10. Bagaimana cara melihat character beserta senjata yang di gunakan nya
```
SELECT characters.name_characters, characters.element_characters, weapon.name_weapon
FROM characters
INNER JOIN weapon ON characters.id_weapon = weapon.id_weapon;
```


11. Bagaimana Menghitung Jumlah Character yang ada pada region 
```
SELECT region, COUNT(*) as total_characters
FROM characters
GROUP BY region
order by total_characters;
```


12. Bagaimana cara merata ratakan Bintang kelangkaan berdasarkan element 
```
SELECT element_characters, AVG(characters_star) as rata_rata_star
FROM characters
GROUP BY element_characters ;
```


13. Bagaimana cara melihat character yang dimiliki dan yang tidak di miliki oleh player
```
SELECT characters.id_characters, characters.name_characters, 
    IF(player_characters.id_player IS NULL, 'Tidak Dimiliki', 'Dimiliki') AS status
FROM characters
LEFT JOIN (
    SELECT id_characters, id_player FROM player_characters WHERE id_player = 1001
) AS player_characters
ON characters.id_characters = player_characters.id_characters;
```


14. Bagaimana cara melihat player mempunyai weapon apa aja
```
select pw.id_weapon , pw.id_player, w.name_weapon  
from player_weapon pw 
inner join weapon w  on w.id_weapon  = pw.id_weapon
where pw.id_player = 1001
order by pw.id_player;
```

15. Mengkategorikan player berdasarkan level player
```
SELECT
    id_player,
    username_player,
    CASE
        WHEN level_player < 20 THEN 'Beginner'
        WHEN level_player >= 20 AND level_player < 50 THEN 'Intermediate'
        ELSE 'Advanced'
    END AS player_category
FROM player;
```
